Sample code for the subclass sandbox lecture. To step through the progression you have to switch branches in the following order:

1. `no_subclass`

2. `movement_subclass`

3. `sandbox`
﻿// Designers ask for simple bullets that ignore friends, damage enmies and are destroyed when they hit walls

using System;

namespace SubSand
{
    // Basic bullet class
    public class Bullet : Entity
    {
        public int WallHitSoundId { get; set; }
        public int EnemyHitSoundId { get; set; }
        public Texture ExplosionTexture { get; set; }
        public int Damage { get; set; }
        public float Speed { get; set; }
        public Physics.Body Body { get; set; }

        // All bullet behavior can be captured in one class' methods
        public void Update()
        {
            // Check for collisions
            var collisionBody = Physics.CurrentCollision(Body);
            if (collisionBody != null)
            {
                switch (collisionBody.GetTag())
                {
                    case CollisionTag.Wall:
                        // destroy bullet when it hits a wall
                        Explode();
                        PlayWallHitSound();
                        Destroy();
                        break;
                    case CollisionTag.Enemy:
                        // destroy bullet and damage the enemy when it hits an enemy
                        Explode();
                        PlayEnemyHitSound();
                        collisionBody.GetOwnerAs<Enemy>().ApplyDamage(Damage);
                        Destroy();
                        break;
                    case CollisionTag.Friend:
                        // ignore collision with a friend
                        break;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }

            // Move
            if (!IsDestroyed())
            {
                Body.ApplyImpulse(Body.Velocity.GetNormalized(), Speed);
            }
        }

        private void Destroy()
        {
            EntityManager.Destroy(this);
        }

        private void Explode()
        {
            Particles.GeneratEffect(ExplosionTexture);
        }

        private void PlayWallHitSound()
        {
            Sounds.PlaySound(WallHitSoundId);
        }

        private void PlayEnemyHitSound()
        {
            Sounds.PlaySound(WallHitSoundId);
        }

    }

}
﻿
namespace SubSand
{

    public class Vec2f
    {
        public Vec2f GetNormalized()
        {
            return null;
        }
    }

    public class EntityManager
    {
        public static void Destroy(Entity e)
        {

        }
    }

    public class Entity
    {
        public bool IsDestroyed()
        {
            return false;
        }
    }

    public class Actor : Entity
    {

    }

    public class Enemy : Actor
    {
        public void ApplyDamage(int damage) {}
    }

    public class Friend : Actor
    {

    }

    public enum CollisionTag
    {
        Wall,
        Enemy,
        Friend
    }

    public class Sounds
    {
        public static void PlaySound(int soundId) {}
    }


    public class Physics
    {

        public class Body {
            public CollisionTag GetTag()
            {
                return CollisionTag.Wall;
            }

            public Vec2f Velocity { get; set; }

            public void ApplyImpulse(Vec2f dir, float speed)
            {
                throw new System.NotImplementedException();
            }

            public T GetOwnerAs<T>()
            {
                throw new System.NotImplementedException();
            }
        }

        public static Body CurrentCollision(Body body)
        {
            return null;
        }
    }

    public class Texture
    {

    }

    public class Particles
    {
        public static void GeneratEffect(Texture texture) {}
    }

}